const mongoose = require('mongoose')

const mealSchema = mongoose.Schema({
  date: {
    type: Date,
    required: true
  },
  period: {
    type: String,
    required: true,
    enum: ['LUNCH', 'DINNER']
  },
  menu: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Menu'
  }
})

mealSchema.index({ date: 1, period: 1 }, { unique: true })

const Meal = mongoose.model('Meal', mealSchema, 'Meals')

module.exports = Meal
