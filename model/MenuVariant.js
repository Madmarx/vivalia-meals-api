const mongoose = require('mongoose')

const menuVariantSchema = mongoose.Schema({
  dishes: [{
    type: String,
    required: true
  }],
  diet: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Diet',
    required: true
  },
  menuBase: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Menu',
    required: true
  },
  hash: {
    type: String,
    required: true
  }
})

menuVariantSchema.index({ hash: 1, diet: 1, menuBase: 1 }, { unique: true })

const MenuVariant = mongoose.model('MenuVariant', menuVariantSchema, 'MenuVariants')

module.exports = MenuVariant
