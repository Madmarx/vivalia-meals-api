const mongoose = require('mongoose')

const sideMenuSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  }
})

sideMenuSchema.index({ name: 1 }, { unique: true })

const SideMenu = mongoose.model('SideMenu', sideMenuSchema, 'SideMenus')

module.exports = SideMenu
