const mongoose = require('mongoose')

const pavillonSchema = mongoose.Schema({
  name: {
    type: String,
    trim: true
  },
  sideMenus: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'SideMenu'
    }
  ]
})

pavillonSchema.index({ name: 1 }, { unique: true })

const Pavillon = mongoose.model('Pavillon', pavillonSchema, 'Pavillons')

module.exports = Pavillon
