const mongoose = require('mongoose')

const patientSchema = mongoose.Schema({
    firstname: {
        type: String,
        required: true,
        trim: true
    },
    lastname: {
        type: String,
        required: true,
        trim: true
    },
    pavillon: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pavillon'
    },
    eatingPlace: {
        type: String,
        required: true,
        default: 'SELF',
        enum: ['PAVILLON', 'SELF']
    },
    hasOrdinaryDiet: Boolean,
    diets: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Diet'
    }]
})

patientSchema.index({ firstname: 1, lastname: 1 }, { unique: true })

const Patient = mongoose.model('Patient', patientSchema, 'Patients')


module.exports = Patient
