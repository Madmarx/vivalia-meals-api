const express = require('express')
  router = express.Router() // TODO convert all app.use to router.use?
  session = require('express-session')
  cookieParser = require('cookie-parser')
  bodyParser = require('body-parser')
  path = require('path')
  favicon = require('serve-favicon')
  formidable = require('formidable')
  parseurl = require('parseurl')
  handlebars = require('express-handlebars').create({ defaultLayout: 'skeleton' })
  mongoose = require('mongoose')
  MongoStore = require('connect-mongo')(session)
  flash = require('connect-flash')
  toastr = require('express-toastr')
  cors = require('cors')

  home = require('./routes/index')
  api = require('./routes/api/ApiController')
  connection = require('./routes/connection')
  //views = require('./routes/views')
  diets = require('./routes/diets')
  patients = require('./routes/patients')
  register = require('./routes/register')
  settingsPage = require('./routes/settings')
  credentials = require('./credentials')
  settings = require('./settings')

  logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

var strings = settings.strings

var app = express()


app.disable('x-powered-by')

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.engine('handlebars', handlebars.engine)
app.set('view engine', 'handlebars')


// site setup
app.use(favicon(path.join(__dirname, 'public', 'img/favicon.ico')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(cors())

// db connection
var db = mongoose.connection
mongoose.connect(settings.currentDatabase.url, {
  useMongoClient: true,
  // connectTimeoutMS: 750,
  // socketTimeoutMS: 750,
  // reconnectTries: 10,
  // reconnectInterval: 1000
})
//})

// session & cookie setup
// logger.trace('session fill')
// app.use(cookieParser(credentials.secret)) //
// app.use(session({
//   secret: credentials.secret, // test
//   //resave: false,
//   resave: true,
//   //saveUninitialized: false,
//   saveUninitialized: true,
//   store: new MongoStore({
//     mongooseConnection: db
//   })
// }))

app.use(function (req, res, next) {
  db.on('error', function (err) {
    logger.error('MongoDB connection error: ', err)
    next(err)
  })
  next()
})

// toastr (server side popups rendering)
// app.use(flash())
// app.use(toastr())

// app.use(function (req, res, next) {
//   req.toastr.error("strings.errors.internalError.message", "strings.errors.internalError.title")
//   res.locals.toasts = req.toastr.render()
//   next()
// })

// log every url access
app.use(function (req, res, next) {
  res.locals.logLevel = settings.logLevel // sets logLevel for client side
  logger.trace('URL (' + req.method + ') accessed : ' + req.url)
  next()
})

app.use('/api', api)
// app.use('/register', register)
// app.use('/settings', settingsPage)
// app.use('/connection', connection)
// app.use('/diets', diets)
// app.use('/patients', patients)
app.use('/', home)


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error(req.url + ' Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  // res can already been sent to the client in the api
  if (!res.headersSent) {

    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = settings.env === 'dev' ? err : 'We do not provide errors on ' + settings.env + ' environment'

    if (res.locals.error) {
      logger.error(res.locals.error)
    }

    // render the error page
    if (res.locals.error.status == 404) {
      res.status(res.locals.error.status).render('errors/404', {
        title: strings.errors.notFound.code,
        strings: strings
      })

    } else if (res.locals.error.status == 500) {
      res.status(res.locals.error.status).render('errors/500', {
        title: strings.errors.internalError.code,
        strings: strings
      }) // ne pas afficher si ça vient de l'api

    } else if (typeof (res.locals.error) !== 'string') {
      // generic error message in dev env
      res.status(500).render('errors/error', {
        title: strings.general.error,
        //strings: strings, //TODO remove that
        errorCode: res.locals.error.status || res.locals.error.code,
        errorTitle: strings.general.error,
        errorMessage: res.locals.message
      })
    } else {
      // if we're here, we're not in dev environment thus we don't show error informations to the client 
      res.status(500).render('errors/500', {
        title: strings.errors.internalError.code,
        strings: strings
      })
    }
  }

})

module.exports = app
