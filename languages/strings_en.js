exports.strings = {
    langShortname: "EN",

    app: {
        title: "VivaliaMeals",
        description: "VivaliaMeals is a software developed for the hospital called \"La Clairière\", situated in Bertrix. It aims to replace the current system, tedious and subject to human errors"
    },

    pages: {
        home: "Home",
        connection: "Sign in",
        register: "Sign up",
        profile: "Profile",
        users: "Users",
        diets: "Diets",
        cycles: "Cycles",
        dishes: "Dishes",
        menus: "Menus",
        pavillons: "Pavillons",
        suppMenus: "Supplements",
        patients: "Patients"
    },

    errors: {
        notFound: {
            code: 404,
            title: "Page not found",
            message: "The page you're looking for cannot be found"
        },
        internalError: {
            code: 500,
            title: "Internal error",
            message: "An error has happened on server side"
        },
        missingParameter: {
            code: 520,
            message: "Information missing"
        },
        duplicateKey: {
            code: 521,
            message: "Already exists"
        },
        objectNotFound: {
            code: 522,
            message: "Object not found"
        },
        unmatchPasswords: {
            code: 530,
            message: "Passwords don't match"
        }
    },

    general: {
        firstname: "Firstname",
        lastname: "Lastname",
        login: "Username",
        password: "Password",
        passwordConf: "Password confirmation",
        name: "Name",
        description: "Description",
        language: "Language",
        error: "Error"
    },

    actions: {
        signup: "Sign up",
        signin: "Sign in",
        create: "Create",
        actions: "Actions",
        management: "Management",
    },


    // PAGES
    register: {
        addUserTitle: "Add new user",
    },
    connection: {
        signIn: "Sign in",
    },
    diets: {
        listDiets: "Diets list",
        noDietsTitle: "No diet found",
        noDescription: "No description",
        newDescription: "New description",
        newName: "New name",
        toastTitle: "Diet",
    },
    patients: {
        listPatients: "Patients list",
        newFirstname: "New firstname",
        newLastname: "New lastname",
        noPatientsTitle: "No patient found",
        associateDiet: "Diet(s) of this patient",
        noDietWarning: "Ask a dietitian to add some in order to fill the patient information",
        noDiet: "No diet for this patient",
        toastTitle: "Patient",
    },


    // TOASTS
    toastMessage: {
        add: {
            success: "Successfully added",
            failure: "Cannot be added",
        },
        update: {
            success: "Successfully updated",
            failure: "Cannot be updated",
        },
        delete: {
            success: "Successfully deleted",
            failure: "Cannot be deleted",
        },
        warnings: {
            missingField: "Don't forget to fill all the fields",
        }
    }

}