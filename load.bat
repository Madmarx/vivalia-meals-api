echo off
REM edit db_location at your convenience

set db_host="localhost:27017/vivalia"
REM set db_location="\Users\%USERNAME%\dev\databases\MongoDB\vivalia"
set db_location="X:\dev\databases\MongoDB\vivalia"
set db_scripts=%cd%\database\mongo_scripts.js
REM set db_scripts="\Users\%USERNAME%\dev\repos\VivaliaMeals\database_scripts\db_scripts.js"

echo Bad idea, process will have to be terminated manually. Use Linux instead, fool.

IF NOT EXIST %db_location% (
    echo Folder doesn't exist
    mkdir %db_location%
    echo Folder created

    echo Database creation...
    mongo %db_host% %db_scripts%
    echo done

) ELSE (
    echo Folder already exists, using this database
)

sleep 1

REM @start /b cmd /c 
mongod --dbpath %db_location%
REM @start /b cmd /c nodemon www.js
