
//ENVIRONMENT
// prod won't show precise errors to end user
exports.appPath = __dirname
exports.env = env = 'dev' // 'dev' | 'test' | 'prod'
exports.logLevel = logLevel = 'trace' // 'trace' | 'debug' | 'info' | 'warn' | 'error' | 'fatal' | 'off', each log level includes the next ones but not the previous ones


// LOGGER
const log4js = require('log4js')

var appenders = ['stdout']
if (env !== 'dev') appenders.push('file') // if env test or prod then use a log file

exports.log4jsConfig = log4jsConfig = {
    appenders: {
        'stdout': {
            type: 'stdout',
            layout: {
                type: 'coloured'
            }
        },
        'file': {
            type: 'file',
            filename: 'logs.log'
        }
    },
    categories: {
        default: {
            appenders: appenders,
            level: logLevel
        }
    }
}
log4js.configure(log4jsConfig)
exports.logger = log4js


// WEB SERVER
exports.servers = servers = {
    dev: {
        domain: 'https://localhost:1338/',
        host: 'localhost',
        port: 1338
    },
    test: {
        domain: 'https://localhost:1338/',
        host: 'localhost',
        port: 1338
    },
    prod: {
        domain: 'http://madmarx.servehttp.com:1338/',
        host: 'madmarx.servehttp.com',
        port: 1338
    }
}
exports.currentServer = servers[env]


// DATABASE SERVERS
exports.databases = databases = {
    dev: {
        host: 'localhost',
        port: 27017,
        name: 'vivalia',
        url: 'mongodb://localhost:27017/vivalia'
    },
    test: {
        host: 'localhost',
        port: 27017,
        name: 'vivalia',
        url: 'mongodb://localhost:27017/vivalia'
    },
    prod: {
        host: 'localhost',
        port: 27017,
        name: 'vivalia',
        url: 'mongodb://madmarx.servehttp.com:27017/vivalia'
    }
}
exports.currentDatabase = databases[env]


// LANGUAGE SUPPORT
exports.langs = langs = {
    fr: require('./languages/strings_fr'),
    en: require('./languages/strings_en')
}
exports.strings = langs.fr.strings


// APPLICATION
exports.roles = ['NURSE', 'DIETITIAN', 'DIETITIAN_CHIEF', 'RESPONSIBLE', 'ADMIN']
exports.pavillons = ['3', '4', '5', '6', '7', 'Hôpital de jour', 'Hôpital de semaine']


// VALUES
exports.stringShortMaxLength = 25
exports.stringLongMaxLength = 50