/*
Database should be located in : 
	Linux : ~/dev/databases/mongodb/vivalia
	Windows : \Users\%USERNAME%\dev\databases\MongoDB\vivalia

Database name is :
	vivalia
 */



//use vivalia;

db.createCollection("Diets");
db.createCollection("Patients");
db.createCollection("Pavillons");
db.createCollection("Employees");
db.createCollection("GenerateHistory");
db.createCollection("SuppMenus");
db.createCollection("MenusTypes");
db.createCollection("Menus");
db.createCollection("Cycle");
//db.createCollection("Dish");


db.Diets.insert([
	{	
		"_id" : "1",
		"name" : "Normal",
		"description" : "Régime normal",
		"active" : true
	},
	{	
		"_id" : "2",
		"name" : "Sans porc",
		"description" : "Régime sans porc",
		"active" : true
	},
	{	
		"_id" : "3",
		"name" : "Végétarien",
		"description" : "Régime sans viande",
		"active" : true
	},
	{	
		"_id" : "4",
		"name" : "Sans sel",
		"description" : "Régime sans sel",
		"active" : true
	},
	{	
		"_id" : "5",
		"name" : "Sans fer",
		"description" : "Régime sans fer",
	}
]);

db.Patients.insert([
	{
		"_id" : "1",
		"firstname" : "Jean",
		"lastname" : "Claude",
		"diet" : {
			$ref : "Diets",
			$id  : "1"
		},
		"active" : true
	},
	{
		"_id" : "2",
		"firstname" : "Marie",
		"lastname" : "Claude",
		"diet" : {
			$ref : "Diets",
			$id  : "1"
		},
		"active" : true
	},
	{
		"_id" : "3",
		"firstname" : "Marc",
		"lastname" : "Lothaire",
		"diet" : {
			$ref : "Diets",
			$id  : "3"
		},
		"active" : true
	},
	{
		"_id" : "4",
		"firstname" : "Karl",
		"lastname" : "Marx",
		"diet" : {
			$ref : "Diets",
			$id  : "3"
		},
		"active" : true
	},
	{
		"_id" : "5",
		"firstname" : "Lucie",
		"lastname" : "Marx",
		"diet" : {
			$ref : "Diets",
			$id  : "1"
		},
		"active" : true
	}
]);

db.Pavillons.insert([
	{
		"_id" : "1",
		"number" : "3",
		"patients" : [
			{
				$ref : "Patients",
				$id  : "1"
			},
			{
				$ref : "Patients",
				$id  : "2"
			},
		]
	},

	{
		"_id" : "2",
		"number" : "4",
		"patients" : [
			{
				$ref : "Patients",
				$id  : "3"
			},
			{
				$ref : "Patients",
				$id  : "4"
			},
		]
	},

	{
		"_id" : "3",
		"number" : 7,
		"patients" : [
			{
				$ref : "Patients",
				$id  : "5"
			},
		]

	}
]);

db.Employees.insert([
	{
		"_id" : "1",
		"firstname" : "Jeanne",
		"lastname" : "Demoulin",
		"login" : "AgDe",
		"password" : "AgDe",
		"role" : ["infirmier"],
		"pavillon" : {
			$ref : "Pavillons",
			$id : "2"
		}
	},
	
	{
		"_id" : "2",
		"firstname" : "André",
		"lastname" : "Manoukian",
		"login" : "AnMa",
		"password" : "AnMa",
		"role" : ["dieteticien_chef", "dieteticien"],
		"pavillon" : {
			$ref : "Pavillons",
			$id : "2"
		}
	},
	
	{
		"_id" : "3",
		"firstname" : "Marc",
		"lastname" : "Dujardin",
		"login" : "MaDu",
		"password" : "MaDu",
		"role" : ["dieteticien"],
		"pavillon" : {
			$ref : "Pavillons",
			$id : "2"
		}
	},
	
	{
		"_id" : "4",
		"firstname" : "Paul",
		"lastname" : "Oetker",
		"login" : "PaOe",
		"password" : "PaOe",
		"role" : ["budget"],
		"pavillon" : {
			$ref : "Pavillons",
			$id : "2"
		}
	}
]);

db.GenerateHistory.insert([
	{
		"date" : new ISODate("2017-20-08"),
		"refEmployees" : {
			$ref : "Employees",
			$id : "1"
		}
	} 
]);

db.SuppMenus.insert([
	{
		"label" : "pain gris",
		"quantity" : "2",
		"date" : new ISODate("2017-08-20"),
		"refPavillons" : {
			$ref : "Pavillons",
			$id : "2"
		}
	},
	{
		"label" : "pain blanc",
		"quantity" : "2",
		"date" : new ISODate("2017-08-20"),
		"refPavillons" : {
			$ref : "Pavillons",
			$id : "2"
		}
	},
	{
		"label" : "soupe 1,5L",
		"quantity" : "4",
		"date" : new ISODate("2017-08-20"),
		"refPavillons" : {
			$ref : "Pavillons",
			$id : "2"
		}
	}
]);

db.MenusTypes.insert([
	{
		"_id" : "1",
		"name" : "steack frites",
		"dishes" : [
			{
				"name" : "steack",
			},
			{
				"name" : "frites"
			},
			{
				"name" : "salade"
			},
			{
				"name" : "mayonnaise"
			}
		]
	},
	{
		"_id" : "2",
		"name" : "boulettes liégeoises",
		"dishes" : [
			{
				"name" : "boulettes",
			},
			{
				"name" : "frites"
			},
			{
				"name" : "salade"
			},
			{
				"name" : "sauce liégeoise"
			}
		]
	},
	{
		"_id" : "3",
		"name" : "boulettes sauce tomate",
		"dishes" : [
			{
				"name" : "boulettes",
			},
			{
				"name" : "frites"
			},
			{
				"name" : "salade"
			},
			{
				"name" : "sauce tomate"
			}
		]
	}
]);

db.Cycle.insert([
	{
		"_id" : "1",
		"beginDate" : new ISODate("2017-01-01"),
		"endDate" : new ISODate("2017-02-12"),
		"season" : "hiver"
	},
	{
		"_id" : "2",
		"beginDate" : new ISODate("2017-02-13"),
		"endDate" : new ISODate("2017-03-26"),
		"season" : "hiver"
	},
	{
		"_id" : "3",
		"beginDate" : new ISODate("2017-03-27"),
		"endDate" : new ISODate("2017-05-07"),
		"season" : "été"
	},
	{
		"_id" : "4",
		"beginDate" : new ISODate("2017-05-08"),
		"endDate" : new ISODate("2017-06-18"),
		"season" : "été"
	},
	{
		"_id" : "5",
		"beginDate" : new ISODate("2017-06-19"),
		"endDate" : new ISODate("2017-07-30"),
		"season" : "été"
	},
	{
		"_id" : "6",
		"beginDate" : new ISODate("2017-07-31"),
		"endDate" : new ISODate("2017-09-10"),
		"season" : "été"
	},
	{
		"_id" : "7",
		"beginDate" : new ISODate("2017-09-11"),
		"endDate" : new ISODate("2017-10-22"),
		"season" : "hiver"
	},
	{
		"_id" : "8",
		"beginDate" : new ISODate("2017-10-23"),
		"endDate" : new ISODate("2017-12-03"),
		"season" : "hiver"
	},
	{
		"_id" : "9",
		"beginDate" : new ISODate("2017-12-04"),
		"endDate" : new ISODate("2017-12-31"),
		"season" : "hiver"
	}
]);

db.Menus.insert([
	{
		"_id" : "1",
		"date" : new ISODate("2017-01-20"),
		"period" : "midi",
		"refMenuTypes" : {
			$ref : "MenuTypes",
			$id : "1"
		},
		"refDiets" : {
			$ref : "Diets",
			$id : "1"
		},
		"refCycle" : {
			$ref : "Cycle",
			$id : "1"
		}
		/*
		"refPatients" : {
			$ref : "Patients",
			$id : "2"
		}
		*/
	},
	{
		"_id" : "2",
		"date" : new ISODate("2017-01-20"),
		"period" : "soir",
		"refMenuTypes" : {
			$ref : "MenuTypes",
			$id : "3"
		},
		"refDiets" : {
			$ref : "Diets",
			$id : "1"
		},
		"refCycle" : {
			$ref : "Cycle",
			$id : "1"
		}
	}
]);
