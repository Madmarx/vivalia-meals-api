#/bin/bash
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

DB_HOST="localhost:27017/vivalia"
DB_LOCATION="~/dev/databases/MongoDB/vivalia"
DB_SCRIPTS=$SCRIPTPATH"/database/mongo_scripts.js"

# if your folder doesn't exist then put the database in /tmp folder
if [ ! -d "$DB_LOCATION" ]; then
  $DB_LOCATION="/tmp/vivaliaDB"
fi

# if the folder is empty then initialize database with the given script
if [ -z "$(ls -A $DB_LOCATION)" ]; then
   mongo $DB_HOST $DB_SCRIPTS
fi

mongod --dbpath $DB_LOCATION &
nodemon www.js
