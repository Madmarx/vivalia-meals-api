const settings = require('./settings')

var domain = settings.currentServer.domain

exports.api = {
    users: (domain + "api/users"),
    diets: (domain + "api/diets"),
    patients: (domain + "api/patients"),
    pavillons: (domain + "api/pavillons"),
}