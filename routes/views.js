const express = require('express'),
    router = express.Router()

/* GET views page. */
router.get('/', function (req, res, next) {
    res.send("You viewed this page " + req.session.views['/views'] + " times")
})

module.exports = router