const express = require('express'),
    router = express.Router()
    /*PasswordHash = require('password-hash')*/

const settings = require('../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))


User = require('../model/User')


/* GET connection page. */
router.get('/', function (req, res, next) {
    res.render('connection',
        {
            title: settings.strings.pages.connection,
            strings: settings.strings
        }
    )
})


router.post('/connect', function (req, res, next) {

    var data = {
        login: req.body.login,
        password: req.body.password
    }

    User.getByLogin(data, function (err, user) {
        if (err) {
            // problem(err, req, res)
            logger.err(err)
            next(err)
        } else if (!user) {
            res.status(500)
            next(new Error("No user found"))
        } else {
            if (data.password /*&& PasswordHash.verify(data.password, user.password)*/) {
                res.status(200)
                logger.info("Good login-password")
                //res.redirect(303, "/")
            } else {
                //res.status(501) //custom
                res.status(500)
                next(new Error("Wrong password"))
            }
        }
    })

})

module.exports = router