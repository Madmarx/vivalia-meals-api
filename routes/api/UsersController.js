const express = require('express'),
	router = express.Router(),
	hash = require('sha1')

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))
const credentials = require('../../credentials')
VerifyToken = require('../../utils/verifyToken')

User = require('../../model/User')
Pavillon = require('../../model/Pavillon')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
	User.find({ }, { password: 0 })
		.populate('pavillon')
		.exec((err, objects) => {
			if (err) {
				next(err)
			} else {
				res.json(objects)
			}
		})
})

// GET
router.get('/:id', (req, res, next) => {
	User.findById(req.params.id, { password: 0 })
		.populate('pavillon')
		.exec((err, object) => {
			if (err) next(err)
			if (object !== null) {
				res.json(object)
			} else {
				var err = new Error()
				err.status = settings.strings.errors.objectNotFound.code
				next(err)
			}
		})
})

// NEW
router.put('/', VerifyToken(['ADMIN']), (req, res, next) => {
	if (req.body.username && req.body.username != '' &&
		req.body.password && req.body.password !== '' &&
		req.body.passwordConf && req.body.passwordConf !== '' &&
		req.body.role) {

		var data = {
			firstname: req.body.firstname.trim() || "",
			lastname: req.body.lastname.trim() || "",
			username: req.body.username.trim(),
			password: req.body.password,
			passwordConf: req.body.passwordConf,
			pavillon: req.body.pavillon,
			role: req.body.role
		}

		if (data.password !== data.passwordConf) {
			var err = new Error()
			err.status = settings.strings.errors.unmatchPasswords.code
			next(err)
		} else {
			data.password = hash(data.password + data.username + credentials.salt)
			data.passwordConf = hash(data.passwordConf + data.username + credentials.salt)
			// console.log('encrypted pass: ', data.password)
			// console.log('data: ', data)

			User.create(data, (err, object) => {
				if (err) {
					if (err.code === 11000) {    // duplicate key
						var err = new Error()
						err.status = settings.strings.errors.duplicateKey.code
					}
					next(err)
				} else {
					object.populate('pavillon', (err, result) => {
						if (err) next(err)
						res.json(result)
					})
				}
			})
		}
	} else {
		var err = new Error()
		err.status = settings.strings.errors.missingParameter.code
		next(err)
	}
})

// UPDATE
router.put('/:id', VerifyToken(['ADMIN']), (req, res, next) => {
	var data = {}

	if (req.body.firstname) {
		data.firstname = req.body.firstname.trim()
	}
	if (req.body.lastname) {
		data.lastname = req.body.lastname.trim()
	}
	if (req.body.username && req.body.username != '') {
		data.username = req.body.username.trim()
	}
	if (req.body.password && req.body.passwordConf && req.body.username) {
		data.password = hash(req.body.password + data.username + credentials.salt)
		data.passwordConf = hash(req.body.passwordConf + data.username + credentials.salt)

		if (data.password !== data.passwordConf) {
			var err = new Error()
			err.status = settings.strings.errors.unmatchPasswords.code
			next(err)
		}
	}
	if (req.body.role) {
		data.role = req.body.role
	}
	if (req.body.pavillon) {
		data.pavillon = req.body.pavillon
	}

	User.findByIdAndUpdate(req.params.id, data, { new: true })
		.populate('pavillon')
		.exec((err, object) => {
			if (err) {
				if (err.code === 11000) {    // duplicate key
					var err = new Error()
					err.status = settings.strings.errors.duplicateKey.code
				}
				next(err)
			}
			if (object !== null) {
				res.json(object)
			} else {
				var err = new Error()
				err.status = settings.strings.errors.objectNotFound.code
				next(err)
			}
		})
})


// DELETE
router.delete('/:id', VerifyToken(['ADMIN']), (req, res, next) => {
	User.findByIdAndRemove(req.params.id, (err, object) => {
		if (err) {
			next(err)
		} else {
			if (object !== null) {
				res.json(object)
			} else {
				var err = new Error()
				err.status = settings.strings.errors.objectNotFound.code
				next(err)
			}
		}
	})
})

module.exports = router
