const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')
HashStrings = require('../../utils/hashStrings')

Menu = require('../../model/Menu')
MenuVariant = require('../../model/MenuVariant')
Meal = require('../../model/Meal')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
  Menu.find({}, { hash: 0 }, (err, objects) => {
    if (err) {
      next(err)
    } else {
      res.json(objects)
    }
  })
})

// GET
router.get('/:id', function (req, res, next) {
  Menu.findById(req.params.id, { hash: 0 }, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})

// NEW
router.put('/', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.dishes && req.body.dishes.length > 0) {
    let data = {
      dishes: req.body.dishes,
      hash: HashStrings(req.body.dishes)
    }

    if (req.body.season) {
      data.season = req.body.season
    } // default 'ANY'

    Menu.create(data, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        res.json(object)
      }
    })
  } else {
    let err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})


// UPDATE
router.put('/:id', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.dishes || req.body.season) {
    let data = { }

    if (req.body.dishes && req.body.dishes.length > 0) {
      data.dishes = req.body.dishes
      data.hash = HashStrings(req.body.dishes)
    }
    if (req.body.season) {
      data.season = req.body.season.trim()
    }

    Menu.findByIdAndUpdate(req.params.id, data, { new: true }, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      }
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN', 'DIETITIAN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  Menu.findByIdAndRemove(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        // remove all Meals and MenuVariant
        MenuVariant.deleteMany(
          { menuBase: req.params.id },
          (err) => {
            next(err)
          })
        Meal.deleteMany(
          { menu: req.params.id },
          (err) => {
            next(err)
          })
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})


module.exports = router