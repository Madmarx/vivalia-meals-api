const express = require('express'),
  router = express.Router()

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

VerifyToken = require('../../utils/verifyToken')

Diet = require('../../model/Diet')
MenuVariant = require('../../model/MenuVariant')

//////////////////////////////////////////

// GET ALL
router.get('/', (req, res, next) => {
  Diet.find((err, objects) => {
    if (err) {
      next(err)
    } else {
      res.json(objects)
    }
  })
})

// GET
router.get('/:id', function (req, res, next) {
  Diet.findById(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})

// NEW
router.put('/', VerifyToken(['ADMIN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.name && req.body.name.trim() !== "") {
    let data = {
      name: req.body.name.trim(),
      description: req.body.description || ""
    }

    Diet.create(data, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      } else {
        res.json(object)
      }
    })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})


// UPDATE
router.put('/:id', VerifyToken(['ADMIN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  if (req.body.name && req.body.name.trim() !== '') {
    let data = {
      name: req.body.name,
      description: req.body.description || ''
    }

    Diet.findByIdAndUpdate(req.params.id, data, { new: true }, (err, object) => {
      if (err) {
        if (err.code === 11000) {    // duplicate key
          var err = new Error()
          err.status = settings.strings.errors.duplicateKey.code
        }
        next(err)
      }
      if (object !== null) {
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    })
  } else {
    var err = new Error()
    err.status = settings.strings.errors.missingParameter.code
    next(err)
  }
})

// DELETE
router.delete('/:id', VerifyToken(['ADMIN', 'DIETITIAN_CHIEF']), (req, res, next) => {
  Diet.findByIdAndRemove(req.params.id, (err, object) => {
    if (err) {
      next(err)
    } else {
      if (object !== null) {
        MenuVariant.deleteMany(
          { diet: req.params.id },
          (err) => {
            next(err)
          })
        res.json(object)
      } else {
        var err = new Error()
        err.status = settings.strings.errors.objectNotFound.code
        next(err)
      }
    }
  })
})


module.exports = router