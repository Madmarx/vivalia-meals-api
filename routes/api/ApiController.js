const express = require('express'),
    router = express.Router()
    cors = require('cors')

const settings = require('../../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

const patients = require('./PatientsController'),
    pavillons = require('./PavillonsController'),
    diets = require('./DietsController'),
    lotd = require('./LotdController'),
    meals = require('./MealsController'),
    menus = require('./MenusController'),
    menuVariants = require('./MenuVariantsController'),
    sideMenus = require('./SideMenusController'),
    users = require('./UsersController'),
    history = require('./HistoryDayController'),
    auth = require('./AuthController')

//////////////////////////////////////////

router.options('*', cors())
router.use('/', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
})

router.use('/strings', function (req, res, next) {
    res.json(settings.strings)
})

router.use('/auth', auth)
router.use('/patients', patients)
router.use('/pavillons', pavillons)
router.use('/diets', diets)
router.use('/meals', meals)
router.use('/menus', menus)
router.use('/menu-variants', menuVariants)
router.use('/side-menus', sideMenus)
router.use('/users', users)
router.use('/history', history)
router.use('/lotd', lotd)


router.get('/', function (req, res, next) {
    res.send('<h1>VivaliaMeals API</h1><p>Here you can access /menus, /diets, /dishes, /patients, /pavillons, /users, /suppmenus, /cycles, /users</p>')
})


//////////////////////////////////////////

router.use(function (err, req, res, next) {
    logger.error(err)

    err.status = err.status || 500

    switch (err.status) {
        case 500:
            err.message = settings.strings.errors.internalError.message
            break
        case 520:
            err.message = settings.strings.errors.missingParameter.message
            break
        case 521:
            err.message = settings.strings.errors.duplicateKey.message
            break
        case 522:
            err.message = settings.strings.errors.objectNotFound.message
            break
        case 530:
            err.message = settings.strings.errors.unmatchPasswords.message
            break
    }

    // we don't show too much information in other environments than 'dev'
    if (settings.env !== 'dev') {
        err = new Error()
        err.status = settings.strings.errors.internalError.code
        err.message = settings.strings.errors.internalError.message
    }

    logger.error("Error : " + err.status + " - " + err.message)
    res.status(err.status).json(err)
})



module.exports = router