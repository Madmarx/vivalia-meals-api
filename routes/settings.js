const express = require('express'),
    router = express.Router()

const settings = require('../settings')
const logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))


router.use('/lang/:lang', function (req, res, next) {
    var newLang = req.params.lang.toLowerCase()
    settings.strings = settings.langs[newLang].strings || settings.strings

    logger.info("Language set to : " + newLang)

    res.redirect('back')
})


module.exports = router