const express = require('express'),
    request = require('request')
    urls = require('../urls'),
    settings = require('../settings.js'),
    logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))

var router = express.Router()


router.get('/', function (req, res, next) {

    request.post(urls.api.diets, { timeout: 500 }, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var dietsList = JSON.parse(body)

            if (!dietsList)
                logger.warn("No diet found to associate to a patient, a dietitian should add some in order to allow nurses to add patients")

            request.post(urls.api.patients, { timeout: 500 }, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    res.status(200).render('patients',
                        {
                            title: settings.strings.pages.patients,
                            strings: settings.strings,
                            patients: JSON.parse(body), // patients list
                            diets: dietsList, // available diets
                            settings: settings,
                        }
                    )
                } else {
                    next(error)
                }
            })

        } else {
            next(error)
        }
    })

})

module.exports = router
