const express = require('express')

const settings = require('../settings.js')


var router = express.Router()

router.use('/', function (req, res, next) {

  res.render('register',
    {
      title: settings.strings.pages.register,
      strings: settings.strings
    }
  )
})


module.exports = router
