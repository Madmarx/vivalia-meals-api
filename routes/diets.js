const express = require('express'),
    request = require('request'),
    urls = require('../urls'),
    settings = require('../settings.js'),
    logger = settings.logger.getLogger(__filename.replace(settings.appPath, ''))


var router = express.Router()


router.get('/', function (req, res, next) {


    /*
///////////////////
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
    toastr.error('I do not think that word means what you think it means.', 'Inconceivable!')
*/
    //////////////////////

    /**
     * Gets all diets from database using the api
     */
    request.post(urls.api.diets, {
        timeout: 500
    }, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            res.render('diets', {
                title: settings.strings.pages.diets,
                strings: settings.strings,
                diets: JSON.parse(body), // diets list
                settings: settings,
            })
        } else {
            //res.status(500)
            next(error)
            //next(error || new Error("Error 500"))
            /*
            res.render('errors/500',
                {
                    title: settings.strings.errors.internalError.code,
                    strings: strings
                }
            )
            */
        }
    })
})

module.exports = router