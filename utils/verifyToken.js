const jwt = require('jsonwebtoken')
const credentials = require('../credentials')

const verifyToken = (allowedRoles) => {
  // return a middleware
  return (req, res, next) => {
    const token = req.headers['x-access-token']
    // console.log('token', token)
    if (!token)
      return res.status(403).json({ auth: false, message: 'Vous n\'avez pas fourni de token' })
    
    // verifies secret and checks expiration
    jwt.verify(token, credentials.secret, (err, decoded) => {
      if (err) {
        console.error(err)
        return res.status(500).json({ auth: false, message: 'Votre session n\'est plus valide, veuillez vous identifier à nouveau' })
      }
      
      if (allowedRoles.indexOf(decoded.role) > -1) {
        // user is authorized
        next()
      } else {
        res.status(401).json({ code: 401, message: 'Vous n\'êtes pas autorisé' })
      }
    })
  }
}

module.exports = verifyToken